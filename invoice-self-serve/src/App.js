import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import DatePicker from 'material-ui/DatePicker';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import AutoComplete from 'material-ui/AutoComplete';
import Snackbar from 'material-ui/Snackbar';

import ReCAPTCHA from 'react-google-recaptcha';
import fetch from 'isomorphic-fetch';

import InvoiceTable from './InvoiceTable';
import {setLocalStorage, getLocalStorage} from './utils';
import config from './config';
import pluralize from 'pluralize';

import './App.css';

class App extends Component {
  static recaptchaInstance; // Need to think of a better way if possible

  constructor(){
    super();
    let invoices = [];
    this.state = {
      showBookingIdEmptyWarning: false,
      showCheckinEmptyWarning: false,
      showHotelEmptyWarning: false,
      captchaChecked: true,
      fetchingInvoice: false,
      invoiceFetchMessage: "",
      invoiceFetchStatus: false,
      bookingId: "",
      checkin: "",
      hotelCode: "",
      hotelText: "",
      invoices: invoices,
      hotelDetails: [],
      selectedInvoiceIndices: Array(invoices.length).fill(false),
      validEmails: [],
      toastMessage: "",
      toastOpen: false
    }
  }

  componentDidMount(){
    let hotelDetails = getLocalStorage('hotels');
    if(hotelDetails){
      this.setState({ hotelDetails: hotelDetails});
    }
    else{
      fetch(config.hotelDetailsUrl)
      .then(response => {return response.json()}, (err) => (Promise.reject(err)))
      .then(
        response => {
          let hotels = response.data;
          if(hotels && hotels.length > 0){
            hotelDetails = hotels.map((en) => ({name: en.name, code: en.hotelogix_id}));
            setLocalStorage('hotels', hotelDetails);
          }
          this.setState({ hotelDetails: hotelDetails});
        },
        (err) => (Promise.reject(err)) 
      )
      .catch((err) => {
        console.log(err);
      })
    }
  }

  recaptchaVerified = (response) => {
    this.setState({
      captchaChecked: true,
      captchaCode: response
    })
  }

  resetRecaptcha = () => {
    App.recaptchaInstance.reset();
    this.setState({
      captchaChecked: false
    });
  }
  validateInput = () => {
    let {bookingId, hotelCode, hotelText, checkin} = this.state;
    if((!bookingId) || (!checkin)){
      if(!bookingId){
        this.setState({
          showBookingIdEmptyWarning: true
        });
      }
      if(!checkin){
        this.setState({
          showCheckinEmptyWarning: true
        });
      }
      return false;
    }

    if((hotelText) && (!hotelCode)){
      return false;
    }
    return true;
  }

  cleanBookingId = (bookingId) => {
    let newBookingId = '';
    if(bookingId.charAt(0) === 'G'){
      if(bookingId.charAt(1) !== ' '){
        newBookingId = bookingId.substring(0,1) +" "+ bookingId.substring(1);
        return newBookingId;
      }
    }
    return bookingId;
  }

  getInvoice = () => {
    let { bookingId, checkin, captchaCode, hotelCode } = this.state;
    if(this.validateInput()){
      this.setState({
        fetchingInvoice: true,
      });

      fetch(config.fetchInvoiceUrl, {
        method: 'post',
        headers: {
          'Accept': "application/json",
          'Content-Type': 'application/json',
        }, 
        body: JSON.stringify({bookingId: this.cleanBookingId(bookingId), checkin: checkin, captchaCode: captchaCode, hotelCRSId: hotelCode})})
        .then(response => {
            this.setState({
              fetchingInvoice: false,
            });
            if (response.status >= 400) {
              throw new Error("fatt gayi");
            }
            return response.json();
        }, (err) => (Promise.reject(err)) )
        .then(({data}) => {
            this.setState({
              invoices: data,
              selectedInvoiceIndices: Array(data.length).fill(false),
              invoiceFetchMessage: data.length > 0 ? `Found ${data.length} ${pluralize('invoice', data.length)}` : 'No invoices found',
              invoiceFetchStatus: true,
            });
            // this.resetRecaptcha();
        }).catch((err) => {
          this.setState({
            invoiceFetchMessage: "Error generating invoice. Please try again !",
            invoiceFetchStatus: false,
            fetchingInvoice: false,
          });
          // this.resetRecaptcha();
          console.log(err);
        });
      }
  }


  sendMail = (invoiceIds, mailingList) => {
    fetch(config.mailUrl, {
      method: 'post',
      headers: {
        "Accept": "application/json",
        'Content-Type': 'application/json',
  
      },
      body: JSON.stringify({invoiceIds: invoiceIds, emails: mailingList}), 
    })
    .then(response => {
      if (response.status >= 400) {
        throw new Error("fatt gayi");
      }
      return response.json()
    }, (err) => (Promise.reject(err)))
    .then(response => {
        this.setState({
          toastOpen: true,
          toastMessage: "Mail sent successfully !"
        });
    }, (err) => (Promise.reject(err)))
    .catch((err) => {
      this.setState({
        toastOpen: true,
        toastMessage: "Failed to send mail. Please try again !"
      });
      console.log(err);
    });
  }

  onInvoiceSelection = (index, value) => {
    let newIndices = this.state.selectedInvoiceIndices.slice();
    newIndices[index] = value;
    this.setState({
      selectedInvoiceIndices: newIndices
    })

  }
  inputBookingIdChanged = (e) => {
    this.setState({
      bookingId: e.target.value,
      showBookingIdEmptyWarning: true,
    })
  }

  inputCheckinChanged = (field, value) => {
    let date = new Date(value);
    this.setState({
      checkin: date.getFullYear()+ '-' +  (date.getMonth() + 1)  + '-' + date.getDate(),
      showCheckinEmptyWarning: true
    })
  }

  inputHotelSelected = (hotel) => {
    let hotels = this.state.hotelDetails;
    if(hotels && hotels.length > 0){
      let filteredHotel = hotels.filter((en) => (en.name === hotel));
      if(filteredHotel.length > 0){
        this.setState({
          hotelCode: filteredHotel[0].code,
          hotelText: hotel,
          showHotelEmptyWarning: false
        });
      }
    }
  }

  updateHotel = (input) => {
    this.setState({
      hotelCode: "",
      hotelText: input,
      showHotelEmptyWarning: true
    });
  }

  handleRequestClose = () => {
    this.setState({
      toastOpen: false,
      toastMessage: ""
    });
  }

  render() {
    const {showCheckinEmptyWarning,
           showBookingIdEmptyWarning,
           fetchingInvoice,
           invoiceFetchMessage,
           bookingId,
           checkin,
           invoices,
           captchaChecked,
           hotelDetails,
           showHotelEmptyWarning,
           invoiceFetchStatus,
           hotelText,
           selectedInvoiceIndices,
           toastMessage,
           toastOpen,} = this.state;
    let emptyBookingIdWarning = ((showBookingIdEmptyWarning) && (!bookingId))? "Booking ID cannot be empty" : "";
    let emptyCheckinWarning = (showCheckinEmptyWarning) && (!checkin)? "Checkin date cannot be empty" : "";
    let buttonDisabled = (fetchingInvoice) || (!captchaChecked);
    let captchaVisible = (bookingId.length !== 0) && (checkin !== "");

    return (
      <MuiThemeProvider>
        <div className="App">
          <div className="navbar">
            <div className="logo">
              <img src="https://images.treebohotels.com/images/primus/treebo-for-business-logo-for-blackbg.svg" alt="Treebo For Business"/>
            </div>
            <div className="contact-details">

              <div className="email-help">
                <i className="material-icons" style={{color: "white", paddingRight: "8px", paddingBottom: "2px"}}>local_post_office</i>
                <span style={{color: "white"}}> billing@treebohotels.com </span>
              </div>
            </div>
          </div>

          <h2 className="heading-text"> Generate Invoice </h2>

          <div className="flex-input">
            <div className="booking-id">
              <TextField
                hintText="Booking ID (B2B-XXXXXXXXX)"
                errorText={emptyBookingIdWarning}
                onChange={this.inputBookingIdChanged}
                underlineShow={false}
                className="booking-id-input"
                errorStyle={{marginTop: "8px", textAlign: "left"}}
              />
            </div>
            <div className="checkin-date">
              <DatePicker className="date-input"
                          hintText="Checkin Date"
                          style={{textAlign: 'center'}}
                          onChange={this.inputCheckinChanged}
                          mode="landscape"
                          underlineShow={false}
                          autoOk={true}
                          maxDate={new Date()}
                          minDate={(new Date((new Date()).setMonth(new Date().getMonth() - 6)))}/>
              <span className="empty-checkin-warning"> {emptyCheckinWarning} </span>
            </div>
            <div className="hotel">
              <AutoComplete
                className="hotel-input"
                hintText="Hotel name(optional)"
                errorStyle={{marginTop: "8px"}}
                underlineShow={false}
                filter={AutoComplete.fuzzyFilter}
                dataSource={hotelDetails && hotelDetails.length > 0 ? hotelDetails.map((hotel) => (hotel.name)) : []}
                maxSearchResults={10}
                onNewRequest={this.inputHotelSelected}
                onUpdateInput={this.updateHotel}
                hintStyle={{paddingLeft: '8px'}}
                errorText={showHotelEmptyWarning && hotelText? "Hotel must be selected from one of the options" : ""}
              />
              <div style={{display: captchaVisible ? "inherit" : "none"}}>
                {/* <ReCAPTCHA
                  ref={e => App.recaptchaInstance = e}
                  sitekey={config.recaptchaSiteKey}
                  onChange={this.recaptchaVerified}
                  size="normal"
                /> */}
              </div>
            </div>

            <div className="get-invoices-btn">
              <FlatButton 
                className="submit-button"
                color="white"
                backgroundColor={buttonDisabled ? '#dedede' : '#088337'} 
                hoverColor={buttonDisabled ? '#dedede' : 'rgba(6, 128, 52, 0.82)'} 
                label={fetchingInvoice ? "Finding" : "Find Invoice"}
                onClick={this.getInvoice}
                disabled={buttonDisabled}/>
            </div>
          </div>

          <div className="invoice-fetch-message">
            <span style={{color: invoiceFetchStatus ? "#4a4a4a" : "#ed6347"}}>{invoiceFetchMessage}</span>
          </div>

          <div className="results-section">
            {
              invoices && invoices.length > 0?
                (<div className="invoice-table">
                   <InvoiceTable data={invoices}
                                 sendMail={this.sendMail}
                                 selectedInvoiceIndices={selectedInvoiceIndices}
                                 onInvoiceSelection={this.onInvoiceSelection}/>
                 </div>) : (<div className="invoice-table" style={{flexGrow: "2140"}}> </div>)
            }
          </div>

          <Snackbar
            open={toastOpen}
            message={toastMessage}
            autoHideDuration={2000}
            onRequestClose={this.handleRequestClose}
          />
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
