import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import './InvoiceTable.css';

class InvoiceTable extends Component{

  constructor(props){
    super(props);
    this.state = {
      invoiceData: props.data,
      errorIndices: [],
      validEmails: Array(5).fill(""),
      modalOpen: false,
      emailIndices: [],
    }
  }

  onInvoiceSelection = (event, value) => {
    this.props.onInvoiceSelection(event.target.value, value);
  }

  handleOpen = () => {
    this.setState({
      modalOpen: true,
      emailIndices: [1]
    });
  }

  handleClose = () => {
    this.setState({
      modalOpen: false,
      validEmails: Array(5).fill(""),
      errorIndices: [],
      emailIndices: [],
    });
  }

  onEmailChange = (e) => {
    let inputEmail = e.target.value;
    let inputId = parseInt(e.target.id, 10);
    let { validEmails, errorIndices } = this.state;

    if((!inputEmail.match(/^[a-zA-Z0-9._-]+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)) && (inputEmail)){
      let _ = errorIndices.indexOf(inputId) === -1 ? errorIndices.push(inputId) : errorIndices;
      this.setState({
        errorIndices: errorIndices,
      })
    }else{
      validEmails[inputId] = inputEmail;
      this.setState({
        errorIndices: errorIndices.filter((en) => (en !== inputId)),
        validEmails: validEmails
      })
    }
  }

  onSendMail = () => {
    let { invoiceData, validEmails, errorIndices } = this.state;
    let { selectedInvoiceIndices } = this.props;
    let invoiceIds = [];
    for(let i=0;i<selectedInvoiceIndices.length;i++){
      if(selectedInvoiceIndices[i] === true){
        invoiceIds.push(invoiceData[i].invoiceId);
      }
    }
    this.props.sendMail(invoiceIds, validEmails.filter(en => en !== ""));
    if(errorIndices.length === 0){
      this.handleClose();
    }
  }

  onAddEmail = () => {
    let indices = this.state.emailIndices;
    let lastIndex = indices[indices.length-1];
    if(lastIndex <= 4){
      this.setState({
        emailIndices: [...indices, lastIndex+1],
      });
    }
    
  }

	render(){
    let { errorIndices, validEmails, modalOpen, emailIndices } = this.state;
    let { selectedInvoiceIndices } = this.props;
    let invoiceData = this.props.data;
    let sendMailDisabled = (validEmails.filter(en => en !== "").length === 0) || (errorIndices.length > 0);
    let invoiceSelected = selectedInvoiceIndices.filter(en => en === true).length > 0;

		return (<div>
      <Table className="table">
  	    <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
  	      <TableRow className="table-header">
            <TableHeaderColumn className="table-header small-col"></TableHeaderColumn>
  	        <TableHeaderColumn className="table-header med-col">Invoice ID</TableHeaderColumn>
  	        <TableHeaderColumn className="table-header large-col">Hotel</TableHeaderColumn>
  	        <TableHeaderColumn className="table-header large-col">Corporate</TableHeaderColumn>
  	        <TableHeaderColumn className="table-header small-col">Rooms</TableHeaderColumn>
  	        <TableHeaderColumn className="table-header med-col">Amount</TableHeaderColumn>
  	        <TableHeaderColumn className="table-header med-col"></TableHeaderColumn>
  	      </TableRow>
  	    </TableHeader>
  	    <TableBody displayRowCheckbox={false}>
  	    { invoiceData ? invoiceData.map((en, index) => (
          <TableRow key={index} className="table-row">
            <TableRowColumn style={{textAlign: 'center'}} className="table-row-content small-col">
              <Checkbox checked = {selectedInvoiceIndices[index]}
                value={index}
                labelStyle={{color: 'white'}}
                iconStyle={{fill: '#088337'}}
                onCheck={this.onInvoiceSelection}/>
            </TableRowColumn>
  	        <TableRowColumn className="table-row-content med-col">{en.invoiceNumber}</TableRowColumn>
  	        <TableRowColumn className="table-row-content large-col">{en.hotel}</TableRowColumn>
  	        <TableRowColumn className="table-row-content large-col">{en.corporate}</TableRowColumn>
  	        <TableRowColumn className="table-row-content small-col">{en.rooms}</TableRowColumn>
  	        <TableRowColumn className="table-row-content med-col">{en.amount}</TableRowColumn>
  	        <TableRowColumn className="table-row-content med-col"><a href={en.pdfLink} target="_blank" style={{textDecoration: 'none', color: '#5768e9', fontSize: '14px'}}> Download </a></TableRowColumn>
  	      </TableRow>)) : null
  		  }
  	    </TableBody>
  	  </Table>
      <div className="mail-modal">

        <div className="send-mail-modal">
          <div>
            <div className="send-mail">
              <FlatButton 
                className="send-mail-button"
                color="#088337"
                style={{textTransform: null, border:"2px solid #088337"}}
                backgroundColor="white"
                label="Email Invoice"
                onClick={this.handleOpen}
                disabled={!invoiceSelected}
                />
            </div>
            <Dialog
              title={
                <div>
                  Email Invoices 
                  <i className="material-icons"
                     style={{float: "right"}}
                     onClick={this.handleClose}>close</i>
                  
                </div>
              }
              paperClassName="dialog-paper"
              titleStyle={{fontSize: '28px', fontWeight: 'bold', color: '#4a4a4a', paddingBottom: "8px", paddingTop: "0px"}}
              modal={true}
              open={modalOpen}
            >
              <span style={{fontSize: '14px', color: '#aeaeae'}}> You can send invoices to maximum 5 email addresses </span>
              <div style={{marginTop: "16px"}}>
                {
                  (emailIndices.map((index, idx) => (
                    <div key={idx}>
                      <TextField hintText={`Email Address`}
                                 errorStyle={{marginTop: '5px', paddingLeft: '8px'}}
                                 id={`${idx}`}
                                 className="email-input"
                                 type="email"
                                 underlineShow={false}
                                 hintStyle={{top: '14px', paddingLeft: '8px'}}
                                 inputStyle={{height: 'inherit', marginTop: '0px', paddingLeft: '8px'}}
                                 onChange={this.onEmailChange}
                                 errorText={errorIndices.includes(idx) ? "Invalid email" : ""}/>
                    </div>)))
                }
                { emailIndices.length <= 4 ?
                  (<div className="add-email-text" onClick={this.onAddEmail}>
                    + Click to add another email
                  </div>) : null
                }
              </div>
              <div className="button-border-section">
                <hr 
                  style={{border: "0.5px solid #d9d9d9", marginBottom: "32px", marginTop: "28px"}}
                  height="0px"
                />
                <div className="button-section">
                  <FlatButton
                    className="send-mail-action-button"
                    label="Send Email"
                    onClick={this.onSendMail}
                    backgroundColor={sendMailDisabled ?  "#dedede" : "#088337"}
                    disabled={sendMailDisabled}
                    hoverColor={sendMailDisabled ? '#dedede' : 'rgba(8, 131, 55, 0.78)'}
                  />
                </div>
              </div>
            </Dialog>
          </div>
        </div>
      </div>
    </div>);
	}
};

InvoiceTable.propTypes = {
  data: PropTypes.array,
  sendMail: PropTypes.func,
  selectedInvoiceIndices: PropTypes.array
};
export default InvoiceTable;

