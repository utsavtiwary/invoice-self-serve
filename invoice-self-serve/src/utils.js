export function daysDifference(date1, date2){
	return Math.floor(Math.abs(date1.getTime() - date2.getTime())/1000/60/60/24);
}

export function setLocalStorage(key, value){
	localStorage.setItem(key, JSON.stringify({ value: value, date: + new Date()}));
}

export function getLocalStorage(key){
	let item = localStorage.getItem(key);
	if(item){
		let storedItem = JSON.parse(item);
		if(daysDifference(new Date(), new Date(storedItem.date)) > 60){
			console.log("item expired");
			return null;
		}
		return storedItem.value;
	}
	return null;
}