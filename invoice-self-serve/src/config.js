const config = {
  mailUrl: 'https://mint.treebo.com/invoicing/self-serve/send-email/',
  fetchInvoiceUrl: 'https://mint.treebo.com/invoicing/self-serve',
  recaptchaSiteKey: '6Ldc5EsUAAAAAAnQ8trQp7pq_vdrs9zqltuSrgto',
  hotelDetailsUrl: 'https://www.treebo.com/api/v2/hotels/',
};

export default config;